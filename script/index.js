var startMonth = 0;
var dayClicked = null;
var myEvents = localStorage.getItem('myEvents') ? JSON.parse(localStorage.getItem('myEvents')) : [];
var cityName = localStorage.getItem('cityName') ? JSON.parse(localStorage.getItem('cityName')) : [];


const calendar = document.getElementById("calendar");
const newEventForm = document.getElementById("addEventForm");
const showEventsForm = document.getElementById("showEventsForm");
const backgroundForm = document.getElementById("backgroundForm");
const eventInput = document.getElementById("eventInput");
const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const container = document.getElementById("container");
const WeatherApikey = '21d8ddd952ef2ac501ca4be9af64a8ea';


function loadCalendar() {
  calendar.innerHTML = ""; //Clear the calendar before rendering a new month

  var fullDate = new Date();
  
  if(startMonth != 0) {
    fullDate.setDate(1);
    fullDate.setMonth(new Date().getMonth() + startMonth);
  }
  
  var day = fullDate.getDate();
  var month = fullDate.getMonth();
  var year = fullDate.getFullYear();

  document.getElementById("body").style.backgroundImage = "url('./images/img" + (month + 1) + ".jpg')";

  var paddingDays = new Date(year, month, 1).getDay();

  var daysInmonth = new Date(year, month+1, 0).getDate();

  document.getElementById("monthName").innerText = 
    `${fullDate.toLocaleDateString('en-us', { month: 'long' })} ${year}`;


  for(var i=1; i<=(paddingDays + daysInmonth); i++) {
    const daySquare = document.createElement("div");
    daySquare.classList.add("day");

    const dayString = `${i-paddingDays}/${month+1}/${year}`;

    if(i > paddingDays) {
      daySquare.innerHTML = i - paddingDays;
      daySquare.classList.add("dayCalendar");

      var eventsForDayClicked = eventsOnDayClicked(dayString);
      
      for(var j = 0; j < eventsForDayClicked.length; j++) {
        var eventDiv = document.createElement('div');
        eventDiv.classList.add("event");
        eventDiv.innerHTML = eventsForDayClicked[j].title;
        daySquare.appendChild(eventDiv);
      }

      if(i - paddingDays === day && startMonth === 0) {
        daySquare.id = "currentDay"
      }

      daySquare.addEventListener('click', () => openForm(dayString, daySquare));
    } else {
      daySquare.classList.add("padding");
    }

    calendar.appendChild(daySquare);
  }
}


function initButtons() {
  document.getElementById("nextButton").addEventListener('click', () => {
    startMonth++;
    loadCalendar();
  });

  document.getElementById("backButton").addEventListener('click', () => {
    startMonth--;
    loadCalendar();
  });

  document.getElementById("saveButton").addEventListener('click', () => {
    saveEvent();
  });
  
  document.getElementById("cancelButton").addEventListener('click', () => {
    newEventForm.classList.remove("animate__slideInUp");
    newEventForm.classList.add("animate__fadeOutDown");
    setTimeout(() => { closeForm(); }, 700);
  });

  document.getElementsByClassName("deleteButton")[0].addEventListener('click', () => {
    var titleEvent = document.getElementsByClassName("eventText")[0].text;
    deleteEvent(titleEvent);
  });

  document.getElementsByClassName("deleteButton")[1].addEventListener('click', () => {
    var titleEvent = document.getElementsByClassName("eventText")[1].text;
    deleteEvent(titleEvent);
  });

  document.getElementsByClassName("deleteButton")[2].addEventListener('click', () => {
    var titleEvent = document.getElementsByClassName("eventText")[2].text;
    deleteEvent(titleEvent);
  });

  document.getElementById("addButton").addEventListener('click', function(){
    showEventsForm.style.display = 'none';
    showEventsForm.classList.remove("animate__slideInUp");
    newEventForm.style.display = 'block';
    newEventForm.classList.add("animate__slideInUp");
  });

  document.getElementById("closeButton").addEventListener('click', () => {
    showEventsForm.classList.remove("animate__slideInUp");
    showEventsForm.classList.add("animate__fadeOutDown");
    setTimeout(() => { closeForm(); }, 700);
  });
}


function openForm(date, element) {
  dayClicked = date;
  const existsEventOnDay = myEvents.find(e => e.date === dayClicked);

  var eventsForDayClicked = eventsOnDayClicked(dayClicked);
  
  if(existsEventOnDay) {
    for(var i=0; i<eventsForDayClicked.length; i++) {
      document.getElementsByClassName("eventText")[i].innerHTML = eventsForDayClicked[i].title;
    }
    newEventForm.style.display = 'none';
    showEventsForm.style.display = 'block';
    showEventsForm.classList.remove("animate__fadeOutDown");
    showEventsForm.classList.add("animate__slideInUp");

    var eventsOnDay = myEvents.filter(function(obj) {return obj.date == dayClicked}).length;
    if(eventsOnDay == 1) {
      document.getElementById("eventItem2").style.display = "none";
      document.getElementById("eventItem3").style.display = "none";
      document.getElementById("addButton").style.display = "block";
      document.getElementById("closeButton").style.marginLeft = "74%";
    }

    if(eventsOnDay == 2) {
      document.getElementById("eventItem2").style.display = "block";
      document.getElementById("eventItem3").style.display = "none";
      document.getElementById("addButton").style.display = "block";
      document.getElementById("closeButton").style.marginLeft = "74%";
    }

    if(eventsOnDay == 3) {
      document.getElementById("eventItem3").style.display = "block";
      document.getElementById("addButton").style.display = "none";
      document.getElementById("closeButton").style.marginLeft = "43%";
    }
  } else {
    newEventForm.style.display = 'block';
    newEventForm.classList.remove("animate__fadeOutDown");
    newEventForm.classList.add("animate__slideInUp");
  }

  element.classList.add("daySelected");
  backgroundForm.style.display = 'block';
}

function eventsOnDayClicked(dayClicked) {
  var eventsForDayClicked = [];
  for(var i = 0; i < myEvents.length; i++) {
    if(myEvents[i].date == dayClicked) {
      eventsForDayClicked.push(myEvents[i]);
    }
  }
  return eventsForDayClicked;
}

function closeForm() {
  eventInput.classList.remove("error");
  newEventForm.style.display = 'none';
  newEventForm.classList.remove("animate__fadeOutDown");
  backgroundForm.style.display = 'none';
  showEventsForm.style.display = 'none';
  showEventsForm.classList.remove("animate__fadeOutDown");
  eventInput.value = "";
  if(document.getElementsByClassName("daySelected")[0]) {
    document.getElementsByClassName("daySelected")[0].classList.remove("daySelected");
  }
  dayClicked = null;
  loadCalendar();
}


function saveEvent() {
  if(eventInput.value) {
    eventInput.classList.remove("error");

    myEvents.push({
      date: dayClicked,
      title: eventInput.value,
    })

    localStorage.setItem('myEvents', JSON.stringify(myEvents));

    closeForm();
  } else {
    eventInput.classList.add("error");
  }
}


function deleteEvent(titleEvent) {
  var eventsAfterDeleted = [];
  for(var i = 0; i < myEvents.length; i++) {
    if(myEvents[i].date == dayClicked && myEvents[i].title == titleEvent) {
      continue;
    }
    eventsAfterDeleted.push(myEvents[i]);
  }
  console.log(eventsAfterDeleted);
  myEvents = eventsAfterDeleted;
  localStorage.setItem('myEvents', JSON.stringify(myEvents));
  closeForm();
}


function KelvinToCelcius(tempK) {
  var tempC = tempK - 273.15;
  return tempC;
}

function CelciusToFarenheit(tempC) {
  var tempF = tempC * 9/5 + 32;
  return tempF;
}


async function getCities() {
  var cityList = document.getElementById("cityList");
  cityList.innerHTML = "";

  var input = document.getElementById("city").value;
  var base = 'http://geodb-free-service.wirefreethought.com/v1/geo/cities';
  var query = `?hateoasMode=false&limit=5&offset=0&namePrefix=${input}`;
  var req = new Request(base + query);
  var resp = await fetch(req)
    .then(response => {return response.json()})
    .catch(err => console.error(err));

  for(var i=0; i<resp.data.length; i++) {
    var cityOption = document.createElement("option");
    cityOption.innerHTML = resp.data[i].region;
    cityOption.value = resp.data[i].name + "," + resp.data[i].countryCode;
    cityList.appendChild(cityOption);
  }
}


async function getWeather() {
  var weather = document.getElementById("weather");
  var widget = document.getElementById("widget");
  widget.innerHTML = "";

  cityName = localStorage.getItem('cityName') ? JSON.parse(localStorage.getItem('cityName')) : [];
  if(cityName.length > 0) {
    weather.style.marginTop = '-200px'
    var input = cityName ? cityName[0].name : "";
    var base = 'https://api.openweathermap.org/data/2.5/weather';
    var query = `?q=${input}&appid=${WeatherApikey}`;
    var req = new Request(base + query);
    var resp = await fetch(req)
      .then(response => {return response.json()})
      .catch(err => console.log(err));


    var cityName = resp.name;
    var iconWeather = resp.weather[0].icon;
    var description = resp.weather[0].description;
    var temperatureK = resp.main.temp;
    var temperatureC = KelvinToCelcius(temperatureK);
    var temperatureF = CelciusToFarenheit(temperatureC);
    var temperatureMinK = resp.main.temp_min;
    var temperatureMinC = KelvinToCelcius(temperatureMinK);
    var temperatureMinF = CelciusToFarenheit(temperatureMinC);
    var temperatureMaxK = resp.main.temp_max;
    var temperatureMaxC = KelvinToCelcius(temperatureMaxK);
    var temperatureMaxF = CelciusToFarenheit(temperatureMaxC);

    var cityNameElement = document.createElement("h2");
    var temperatureElement = document.createElement("h2");
    var temperatureMinMaxDiv = document.createElement("div");
    var temperatureMinElement = document.createElement("p");
    var temperatureMaxElement = document.createElement("p");
    var imageElement = document.createElement("img");
    var descriptionElement = document.createElement("h3");

    cityNameElement.innerHTML = cityName;
    temperatureElement.innerHTML = temperatureC.toFixed(1) + "°C / " + temperatureF.toFixed(1) + "°F";
    temperatureElement.classList.add("TempNow");
    temperatureMinElement.innerHTML = "Min: " + temperatureMinC.toFixed(1) + "°C / " + temperatureMinF.toFixed(1) + "°F";
    temperatureMaxElement.innerHTML = "Max: " + temperatureMaxC.toFixed(1) + "°C / " + temperatureMaxF.toFixed(1) + "°F";
    temperatureMinMaxDiv.classList.add("TempMinMax");
    imageElement.src = `http://openweathermap.org/img/wn/${iconWeather}@4x.png`;
    descriptionElement.innerHTML = description;
    
    widget.appendChild(cityNameElement);
    widget.appendChild(temperatureElement);
    temperatureMinMaxDiv.appendChild(temperatureMinElement);
    temperatureMinMaxDiv.appendChild(temperatureMaxElement);
    widget.appendChild(temperatureMinMaxDiv);
    widget.appendChild(imageElement);
    widget.appendChild(descriptionElement);

    widget.style.display = 'block';
    document.getElementById("searchCity").style.display = 'block';
  }
}


function updateCityName() {
  var cityInput = document.getElementById("city").value;
  var newCityInput = [];
  if(cityInput) {
    newCityInput.push({
      name: cityInput
    })

    cityName = newCityInput;
    localStorage.setItem('cityName', JSON.stringify(cityName));
    getWeather();
  }
}

initButtons();
loadCalendar();
getWeather();